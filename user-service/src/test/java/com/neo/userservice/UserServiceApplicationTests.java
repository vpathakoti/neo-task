package com.neo.userservice;

import com.neo.api.controller.UserController;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
public class UserServiceApplicationTests {
    @InjectMocks
    private UserController userController;
    @Autowired
    private MockMvc mockMvc;

    @Before(value = "")
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }
    /*
     * Test validation for empty request body
     *
     */
/*    @Test
    public void testEmptyRequestBody() throws Exception {
        MvcResult mvcResult =
                mockMvc
                        .perform(
                                post("/api/v1/saveUser")
                                        .accept(MediaType.APPLICATION_JSON)
                                        .content("")
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }*/

    /*
     * Test for create the user
     *
     */
/*    @Test
    public void testCreateUser() throws Exception {
        MvcResult mvcResult =
                mockMvc
                        .perform(
                                post("/api/v1/saveUser")
                                        .accept(MediaType.APPLICATION_JSON)
                                        .content("
                                        "{\n"
                                        + "\"id\": \22\,\n"
                                        + "\"firstName\": \"Venkatesh\",\n"
                                       + "\"lastName\": \"Pathakoti\",\n"
                                        ")
                                        .contentType(MediaType.APPLICATION_JSON))
                        .andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }*/

}
