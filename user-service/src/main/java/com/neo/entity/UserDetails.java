package com.neo.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "user_detail1", schema = "neo")
public class UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "first_name", nullable = false)
    private String firstName;
    @Column(name = "last_name", nullable = false)
    private String lastName;

    /**
     * Following four params are optional, for more
     * readable and best practice adding for any table creation for an audit,
     * As per the task commenting below code.
     */
/*    @Column(name = "created_Date", nullable = true)
    private String createdDate;
    @Column(name = "updated_Date", nullable = true)
    private String updatedDate;
    @Column(name = "created_by", nullable = true)
    private long createdBy;
    @Column(name = "updated_By", nullable = true)
    private long updatedBy;*/
}
