package com.neo.repository;

import com.neo.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<UserDetails, Long > {

    Optional<UserDetails> getUserDetailsByUserId(Long userId);

    void deleteUserDetailsByUserId(Long userId);

}
