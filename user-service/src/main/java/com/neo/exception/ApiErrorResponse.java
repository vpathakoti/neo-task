package com.neo.exception;

import lombok.Data;

import java.io.Serializable;

/**
 * ApiErrorResponse.
 *
 * @author 145189
 */
@Data
public class ApiErrorResponse {

  private ErrorDetails error;

  @Data
  public static class ErrorDetails implements Serializable {

    private static final long serialVersionUID = -7594607849233901595L;

    private String timestamp;
    private String status;
    private int code;
    private String details;
    private String path;
  }
}
