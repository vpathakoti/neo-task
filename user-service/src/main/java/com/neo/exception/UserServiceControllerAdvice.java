package com.neo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

@RestControllerAdvice
public class UserServiceControllerAdvice extends ResponseEntityExceptionHandler {

    /**
     * Handle the exception when getting the resource Not found error.
     *
     * @param exception SNSPublisherServiceException
     * @param request   HttpServletRequest.
     * @return ApiErrorResponse
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ApiErrorResponse handleResourceNotFound(
            final ResourceNotFoundException exception, final HttpServletRequest request) {
        final ApiErrorResponse errorResponse = new ApiErrorResponse();

        ApiErrorResponse.ErrorDetails errorDetails = new ApiErrorResponse.ErrorDetails();
        errorDetails.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        errorDetails.setDetails(exception.getMessage());
        errorDetails.setPath(request.getRequestURI());
        errorDetails.setStatus(HttpStatus.NOT_FOUND.getReasonPhrase());
        errorDetails.setCode(HttpStatus.NOT_FOUND.value());

        errorResponse.setError(errorDetails);

        return errorResponse;
    }




    /**
     * Handle the exception when user not authorized.
     *
     * @param exception UnauthorizedException
     * @param request   HttpServletRequest.
     * @return ApiErrorResponse
     */
    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public ApiErrorResponse handleUnauthorizedExceptions(
            final UnauthorizedException exception, final HttpServletRequest request) {
        final ApiErrorResponse errorResponse = new ApiErrorResponse();

        ApiErrorResponse.ErrorDetails errorDetails = new ApiErrorResponse.ErrorDetails();
        errorDetails.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        errorDetails.setDetails(exception.getLocalizedMessage());
        errorDetails.setPath(request.getRequestURI());
        errorDetails.setStatus(HttpStatus.UNAUTHORIZED.getReasonPhrase());
        errorDetails.setCode(HttpStatus.UNAUTHORIZED.value());

        errorResponse.setError(errorDetails);

        return errorResponse;
    }

    /**
     * Handle the exception when client gets the bad request error.
     *
     * @param exception ResourceAlreadyExistsException
     * @param request   HttpServletRequest.
     * @return ApiErrorResponse
     */
    @ExceptionHandler(ResourceAlreadyExistsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ApiErrorResponse handleUserAlreadyExistsExceptions(
            final ResourceAlreadyExistsException exception, final HttpServletRequest request) {
        final ApiErrorResponse errorResponse = new ApiErrorResponse();

        ApiErrorResponse.ErrorDetails errorDetails = new ApiErrorResponse.ErrorDetails();
        errorDetails.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        errorDetails.setDetails(exception.getMessage());
        errorDetails.setPath(request.getRequestURI());
        errorDetails.setStatus(HttpStatus.BAD_REQUEST.getReasonPhrase());
        errorDetails.setCode(HttpStatus.BAD_REQUEST.value());

        errorResponse.setError(errorDetails);

        return errorResponse;
    }

    /**
     * Handle the exception when client gets internal server error.
     *
     * @param exception Exception
     * @param request   HttpServletRequest.
     * @return ApiErrorResponse
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public ApiErrorResponse handleException(
            final Exception exception, final HttpServletRequest request) {

        final ApiErrorResponse errorResponse = new ApiErrorResponse();
        ApiErrorResponse.ErrorDetails errorDetails = new ApiErrorResponse.ErrorDetails();
        errorDetails.setTimestamp(new Timestamp(System.currentTimeMillis()).toString());
        errorDetails.setDetails(exception.getMessage());
        errorDetails.setPath(request.getRequestURI());
        errorDetails.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
        errorDetails.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());

        errorResponse.setError(errorDetails);

        return errorResponse;
    }
}