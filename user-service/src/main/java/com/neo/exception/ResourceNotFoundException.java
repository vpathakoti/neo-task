package com.neo.exception;

import org.springframework.core.NestedRuntimeException;

public class ResourceNotFoundException extends NestedRuntimeException {
    public ResourceNotFoundException(String msg) {
        super(msg);
    }

    public ResourceNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
