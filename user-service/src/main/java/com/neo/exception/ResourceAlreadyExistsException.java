package com.neo.exception;

import org.springframework.core.NestedRuntimeException;

public class ResourceAlreadyExistsException extends NestedRuntimeException {
    public ResourceAlreadyExistsException(String msg) {
        super(msg);
    }

    public ResourceAlreadyExistsException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
