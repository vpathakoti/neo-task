package com.neo.exception;

import org.springframework.core.NestedRuntimeException;

public class UnauthorizedException extends NestedRuntimeException {
    public UnauthorizedException(String msg) {
        super(msg);
    }

    public UnauthorizedException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
