package com.neo.service.impl;

import com.neo.entity.UserDetails;
import com.neo.exception.ResourceAlreadyExistsException;
import com.neo.exception.ResourceNotFoundException;
import com.neo.repository.UserRepo;
import com.neo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepo userRepo;

    /**
     * Gwt the user list.
     * @return List of Users.
     */
    @Override
    public List<UserDetails> getAllUsers() {
        return userRepo.findAll();
    }

    /**
     *Get the user by ID.
     * @param userId
     * @return User.
     * @throws ResourceNotFoundException
     */
    @Override
    public Optional<UserDetails> getUser(long userId) throws ResourceNotFoundException {
        return Optional.ofNullable(userRepo.getUserDetailsByUserId(userId).orElseThrow(() ->
                new ResourceNotFoundException("User does not exist with userId" + userId)));
    }

    /**
     * Save the user.
     * @param userDetails
     * @return User.
     */
    @Override
    public Optional<UserDetails> createUser(UserDetails userDetails) {
        List<UserDetails> userList;
        List<UserDetails> filteredUserList;
        userList = userRepo.findAll();
        filteredUserList = userList.stream().filter(user ->
                user.getFirstName().concat(user.getLastName()).equalsIgnoreCase(userDetails.getFirstName().concat(userDetails.getLastName()))).collect(Collectors.toList());
        if(!filteredUserList.isEmpty()){
            throw  new ResourceAlreadyExistsException("User already exists with userId " +userDetails.getFirstName()+userDetails.getLastName());
        }
        //userDetails.setCreatedDate(LocalDateTime.now().toString());
        //userDetails.setUpdatedDate(LocalDateTime.now().toString());
        //userDetails.setCreatedBy(userDetails.getId());
        //userDetails.setUpdatedBy(userDetails.getId());
        return Optional.of(userRepo.save(userDetails));
    }

    /**
     * Update the user.
     * @param userId
     * @param userDetails
     * @return user.
     */
    @Override
    public Optional<UserDetails> updateUser(long userId, UserDetails userDetails) {
        Optional<UserDetails> userModelFromDb;
        userModelFromDb = userRepo.getUserDetailsByUserId(userId);
        if(!userModelFromDb.isPresent()){
            throw  new ResourceNotFoundException("User does not exist with userId " +userId);
        }
        //userModelFromDb.get().setUpdatedBy(userDetails.getId());
       // userModelFromDb.get().setUpdatedDate(LocalDateTime.now().toString());
        userModelFromDb.get().setFirstName(userDetails.getFirstName());
        userModelFromDb.get().setLastName(userDetails.getLastName());

        return Optional.of(userRepo.save(userModelFromDb.get()));
    }

    /**
     * Delete the user by ID.
     * @param userId
     */
    @Override
    public void deleteUser(long userId) {
        Optional<UserDetails> userModelFromDb;
        userModelFromDb = userRepo.getUserDetailsByUserId(userId);
        if(!userModelFromDb.isPresent()){
            throw  new ResourceNotFoundException("User does not exist with userId " +userId);
        }
        userRepo.deleteUserDetailsByUserId(userId);
    }
}
