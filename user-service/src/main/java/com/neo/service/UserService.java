package com.neo.service;

import com.neo.entity.UserDetails;
import com.neo.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

/**
 * User service
 */
public interface UserService {

    List<UserDetails> getAllUsers();
    Optional<UserDetails> getUser(long userId) throws ResourceNotFoundException;
    Optional<UserDetails> createUser(UserDetails userModel);
    Optional<UserDetails> updateUser(long userId, UserDetails userDetails);
    void deleteUser(long userId);
}
