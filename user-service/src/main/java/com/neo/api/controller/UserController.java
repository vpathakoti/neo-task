/**
 * .
 */
package com.neo.api.controller;

import com.neo.entity.UserDetails;
import com.neo.service.UserService;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * User controller.
 */

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class UserController {

    /**
     * Inject the user service.
     */
    @Autowired
    private UserService userService;

    /**
     * Get all the users and sort the user by last name.
     * @return list of users.
     */
    @GetMapping(
            value = "/users",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDetails>> getUserList() {
        log.info("get all users");
        List<UserDetails>  userDetailsList = userService.getAllUsers().stream()
                .sorted(Comparator.comparing(UserDetails::getLastName))
                .collect(Collectors.toList());
        return new ResponseEntity<>(userDetailsList, HttpStatus.OK);
    }

    /**
     * Save/Persist the user.
     * @param userDetails
     * @return User.
     */
    @PostMapping(
            value = "saveUser",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<UserDetails>> createUser(
            final @Validated @RequestBody UserDetails userDetails) {
        log.info("Create the user {} ", userDetails);
        Optional<UserDetails> userDetails1;
        userDetails1 = userService.createUser(userDetails);
        return  new ResponseEntity<>(userDetails1, HttpStatus.OK);

    }

    /**
     * Get the user.
     * @param userId
     * @return User.
     */
    @GetMapping(
            value = "getUser/{userId}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<UserDetails>> getUser(
            final @PathVariable long userId) {
        log.info("get User details {} ", userId);
        Optional<UserDetails> userDetails1 = Optional.empty();
        userDetails1 = userService.getUser(userId);
        return  new ResponseEntity<>(userDetails1, HttpStatus.OK);
    }

    /**
     * Update the User.
     * @param userDetails
     * @param userId
     * @return User.
     */
    @PatchMapping(
            value = "updateUser/{userId}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<UserDetails>> updateUser(
            final @Validated @RequestBody UserDetails userDetails,
            final @PathVariable long userId) {
        log.info("update the user {} ", userDetails, "For UserId {}", userId);
        Optional<UserDetails> userDetails1;
        userDetails1 = userService.updateUser(userId, userDetails);
        return  new ResponseEntity<>(userDetails1, HttpStatus.OK);

    }

    /**
     * Delete the User.
     * @param userId
     * @return Status.
     */
    @DeleteMapping(
            value = "deleteUser/{userId}",
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Optional<String>> deleteUser(
            final @PathVariable long userId) {
        log.info("Delete the user For UserId {}", userId);
        Optional<String> status;
        userService.deleteUser(userId);
        status = Optional.of("User deleted successfully");
        return  new ResponseEntity<>(status, HttpStatus.OK);

    }
}
